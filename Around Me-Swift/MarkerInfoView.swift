//
//  MarkerInfoView.swift
//  Feed Me
//
//  Created by Ashima on 2/6/16.
//  Copyright © 2016 Ashima. All rights reserved.
//

import UIKit

class MarkerInfoView: UIView {
  
  @IBOutlet weak var placePhoto: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
}
