//
//  DemoData.swift
//  JustDialBase
//
//  Created by Ashima on 2/6/16.
//  Copyright © 2016 Ashima. All rights reserved.
//

import UIKit
import ObjectMapper

class DemoData : Mappable {
    var docId : AnyObject?
    var name : AnyObject?
    var address : AnyObject?
    var thumbnail : AnyObject?
    var NewAddress : AnyObject?
    var totJdReviews : AnyObject?
    
    required init?(_ map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        docId    <- map["docId"]
        name         <- map["name"]
        address      <- map["address"]
        thumbnail       <- map["thumbnail"]
        totJdReviews  <- map["totJdReviews"]
        NewAddress  <- map["NewAddress"]
        
    }

}
